{
    "name": "veracode",
    "displayName": "Veracode",
    "description": "Veracode Scan result viewer",
    "version": "0.0.6",
    "publisher": "buzzcode",
    "engines": {
        "vscode": "^1.34.0"
    },
    "repository": {
        "type": "git",
        "url": "https://gitlab.com/buzzcode/VSCode-Veracode.git"
    },
    "license": "MIT",
    "categories": [
        "Other"
    ],
    "activationEvents": [
        "onCommand:veracodeStaticExplorer.refresh",
        "onView:veracodeStaticExplorer"
    ],
    "main": "./out/extension",
    "contributes": {
        "configuration": {
            "type": "object",
            "title": "Veracode",
            "properties": {
                "veracode.API profile in configuration file": {
                    "$id": "securityProfile",
                    "type": "string",
                    "default": "default",
                    "markdownDescription": "The profile (or section) of API credentials to be used for communicating with Veracode Platform.",
                    "scope": "application"
                },
                "veracode.credsFile": {
                    "type": "string",
                    "default": "",
                    "description": "Specifies the absolute filepath for the API Credentials"
                },
                "veracode.scanCount": {
                    "type": "number",
                    "default": 10,
                    "description": "Number of scans to show for each application"
                },
                "veracode.logLevel": {
                    "type": [
                        "string",
                        "null"
                    ],
                    "default": "info",
                    "description": "Logging level (changes need a restart to take effect)",
                    "enum": [
                        "trace",
                        "debug",
                        "info",
                        "warning",
                        "error",
                        "silent"
                    ]
                },
                "veracode.proxyHost": {
                    "type": "string",
                    "default": "",
                    "description": "Specifies the name of the proxy server (e.g., https://company-proxy.com)"
                },
                "veracode.proxyPort": {
                    "type": "string",
                    "default": "0",
                    "description": "Specifies the port number on the proxy server (e.g., 8080)"
                },
                "veracode.proxyName": {
                    "type": "string",
                    "default": "",
                    "description": "Specifies the login name on the proxy server"
                },
                "veracode.proxyPassword": {
                    "type": "string",
                    "default": "",
                    "description": "Specifies the login password on the proxy server"
                },
                "veracode.sandboxCount": {
                    "type": "number",
                    "default": 5,
                    "description": "Specifies the maximum number of sandboxes to show for each application"
                }
            }
        },
        "views": {
            "explorer": [
                {
                    "id": "veracodeStaticExplorer",
                    "name": "Veracode"
                }
            ]
        },
        "commands": [
            {
                "command": "veracodeStaticExplorer.refresh",
                "title": "Refresh",
                "category": "Veracode",
                "icon": {
                    "light": "resources/light/refresh.svg",
                    "dark": "resources/dark/refresh.svg"
                }
            },
            {
                "command": "veracodeStaticExplorer.sortSeverity",
                "title": "Sort Flaws by Severity",
                "category": "Veracode"
            },
            {
                "command": "veracodeStaticExplorer.sortCwe",
                "title": "Sort Flaws by CWE",
                "category": "Veracode"
            },
            {
                "command": "veracodeStaticExplorer.sortFile",
                "title": "Sort Flaws by File",
                "category": "Veracode"
            },
            {
                "command": "veracodeStaticExplorer.proposeMitigation",
                "title": "Annotations",
                "category": "Veracode"
            }
        ],
        "menus": {
            "view/title": [
                {
                    "command": "veracodeStaticExplorer.refresh",
                    "when": "view == veracodeStaticExplorer",
                    "group": "navigation"
                }
            ],
            "view/item/context": [
                {
                    "command": "veracodeStaticExplorer.sortSeverity",
                    "when": "view == veracodeStaticExplorer",
                    "group": "navigation"
                },
                {
                    "command": "veracodeStaticExplorer.sortCwe",
                    "when": "view == veracodeStaticExplorer",
                    "group": "navigation"
                },
                {
                    "command": "veracodeStaticExplorer.sortFile",
                    "when": "view == veracodeStaticExplorer",
                    "group": "navigation"
                },
                {
                    "command": "veracodeStaticExplorer.proposeMitigation",
                    "when": "view == veracodeStaticExplorer && viewItem == flaw",
                    "group": "mitigations"
                }
            ]
        }
    },
    "scripts": {
        "vscode:prepublish": "npm run compile",
        "compile": "tsc -p ./",
        "watch": "tsc -watch -p ./",
        "test": "npm run compile && node ./out/test/runTest.js"
    },
    "devDependencies": {
        "@types/mocha": "^8.0.3",
        "@types/node": "^14.6.0",
        "mocha": "^8.1.2",
        "typescript": "^4.0.2",
        "vscode-test": "^1.4.0"
    },
    "dependencies": {
        "@types/glob": "^7.1.3",
        "@types/loglevel": "^1.6.3",
        "@types/request": "^2.48.5",
        "@types/sjcl": "^1.0.29",
        "@types/vscode": "^1.48.0",
        "@types/xml2js": "^0.4.5",
        "axios": "^0.19.0",
        "glob": "^7.1.6",
        "loglevel": "^1.6.8",
        "request": "^2.88.2",
        "sjcl": "^1.0.8",
        "xml2js": "^0.4.23"
    }
}
